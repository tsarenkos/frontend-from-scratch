'use strict';

const globalScopeVariable = 'This variable will be available with global context';

(function (document, getVariableFromClosure) {
  const localScopeVariable = 'This one is placing inside closure';

  if (getVariableFromClosure) {
    getVariableFromClosure(localScopeVariable);
  }

  const CLASS_ITEM = 'todo-item';
  const CLASS_EMPTY = 'empty';
  const CLASS_ACTION = 'todo-action';
  const CLASS_INPUT = 'todo-input';
  const CLASS_ITEM_CONTENT = 'todo-content';

  /* @type {Element} */
  let form;
  /* @type {Element} */
  let doneList;
  /* @type {Element} */
  let todoList;

  /**
   * @return {{ content: string, priority: string }}
   */
  function getFormData() {
    const data = {};
    const inputs = Array.from(form.getElementsByClassName(CLASS_INPUT));
    inputs.forEach((input) => {
      data[input.name] = input.value;
    });
    return data;
  }

  function cleanForm() {
    const inputs = Array.from(form.getElementsByTagName('input'));
    inputs.forEach((input) => {
      input.value = '';
    });
  }

  /**
   *
   * @param {Element} list
   */
  function updateListIsEmptyStatus(list) {
    const itemsCount = list.getElementsByClassName(CLASS_ITEM).length;
    if (itemsCount > 0 && list.classList.contains(CLASS_EMPTY)) {
      list.classList.remove(CLASS_EMPTY);
    } else if (itemsCount === 0 && !list.classList.contains(CLASS_EMPTY)) {
      list.classList.add(CLASS_EMPTY);
    }
  }

  function updateListsEmptyStatus() {
    updateListIsEmptyStatus(todoList);
    updateListIsEmptyStatus(doneList);
  }

  /**
   *
   * @param {{ content: string, priority: string }} data
   * @return {Element}
   */
  function createTodoItem(data) {
    const item = document.createElement('div');
    item.classList.add(CLASS_ITEM);
    item.classList.add(data.priority);

    const content = document.createElement('div');
    content.innerText = data.content;
    content.classList.add(CLASS_ITEM_CONTENT);

    const removeButton = document.createElement('button');
    removeButton.innerText = 'Remove';
    removeButton.classList.add(CLASS_ACTION);
    removeButton.addEventListener('click', function () {
      item.remove();
      updateListsEmptyStatus();
    });

    const doneButton = document.createElement('button');
    doneButton.innerText = 'Done';
    doneButton.classList.add(CLASS_ACTION);
    doneButton.addEventListener('click', function () {
      doneList.append(item);
      updateListsEmptyStatus();
    });

    item.append(content, doneButton, removeButton);
    return item;
  }

  function handleTodoCreate(event) {
    event.preventDefault();
    const todoItemData = getFormData();
    todoList.append(createTodoItem(todoItemData));
    updateListsEmptyStatus();
    cleanForm();
  }

  function init() {
    form = document.getElementById('CreateTodoForm');
    todoList = document.getElementById('TodoList');
    doneList = document.getElementById('DoneList');

    form.addEventListener('submit', handleTodoCreate);
  }


  document.addEventListener('DOMContentLoaded', init);
})(document, console.log);


