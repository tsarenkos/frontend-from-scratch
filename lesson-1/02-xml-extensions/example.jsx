

export const MyComponent = ({ a, b, children }) => (
  <section>
    <div>This is React component markup!</div>
    <div>Feel a free to use javascript here: {a} + {b} = {a+b}</div>
    {children}
  </section>
);

export const AnotherComponent = () => (
  <MyComponent a={2} b={18}>
    <p>I rendered by "AnotherComponent"!</p>
  </MyComponent>
);

/* TODO: rendering result
  <section>
    <div>This is React component markup!</div>
    <div>Feel a free to use javascript here: 2 + 18 = 20</div>
    <p>I rendered by "AnotherComponent"!</p>
  </section>
*/
